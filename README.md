# JDK 14 安装与 Java 环境变量配置指南

本仓库提供了一个详细的指南，帮助用户安装 JDK 14 并配置 Java 环境变量。以下是具体步骤：

## 1. 下载 JDK 14
首先，从官方网站或其他可信来源下载 JDK 14 的安装包。确保选择适合您操作系统的版本。

## 2. 安装 JDK 14
1. 运行下载的安装包。
2. 按照安装向导的提示完成安装过程。
3. 记下 JDK 的安装路径，通常为 `C:\Program Files\Java\jdk-14`（Windows）或 `/usr/lib/jvm/jdk-14`（Linux）。

## 3. 配置 Java 环境变量
### Windows 系统
1. 右键点击“此电脑”或“计算机”，选择“属性”。
2. 点击“高级系统设置”。
3. 在“系统属性”窗口中，点击“环境变量”。
4. 在“系统变量”部分，找到并选择“Path”，然后点击“编辑”。
5. 在“编辑环境变量”窗口中，点击“新建”，然后输入 JDK 的 `bin` 目录路径，例如 `C:\Program Files\Java\jdk-14\bin`。
6. 点击“确定”保存更改。

### Linux 系统
1. 打开终端。
2. 编辑 `~/.bashrc` 或 `~/.bash_profile` 文件，添加以下行：
   ```bash
   export JAVA_HOME=/usr/lib/jvm/jdk-14
   export PATH=$JAVA_HOME/bin:$PATH
   ```
3. 保存文件并关闭编辑器。
4. 运行 `source ~/.bashrc` 或 `source ~/.bash_profile` 使更改生效。

## 4. 验证安装
打开命令提示符（Windows）或终端（Linux），输入以下命令：
```bash
java -version
```
如果安装成功，您将看到类似以下的输出：
```
java version "14"
Java(TM) SE Runtime Environment (build 14+36)
Java HotSpot(TM) 64-Bit Server VM (build 14+36, mixed mode, sharing)
```

## 5. 常见问题
- 如果环境变量配置不正确，可能会导致 `java` 命令无法识别。请检查路径配置是否正确。
- 如果安装过程中遇到问题，请参考官方文档或社区支持。

通过以上步骤，您应该能够成功安装 JDK 14 并配置 Java 环境变量。如有任何问题，欢迎在仓库中提出。